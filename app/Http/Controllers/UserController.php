<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use App\User;
use Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userlist = User::orderBy('rol', 'asc')->orderBy('name', 'asc')->paginate(10);
        return view('back/users/index')
                ->withUserlist($userlist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
        [
            'name' => 'required',
            'phone' => 'required|min:9|max:9'
            ,'email' => 'required|email'
        ]);

        $admin = $request->adminSwitch == 'on' ? 'admin' : '';
        $email = $request->emailSwitch == 'on' ? 1 : 0;

        $usr = User::find($id);

        $usr->phone = $request->phone;
        $usr->email = $request->email;
        $usr->name = $request->name;
        if($usr->id != 'f2ed6bc0-9290-11e7-b764-bb4e940e832d'){
            $usr->rol = $admin;
            $usr->confirmed = $email;
        }

        $usr->save();

        Session::flash('success', "El usuari s'ha modificat correctament" );

        return redirect()->route('u.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
