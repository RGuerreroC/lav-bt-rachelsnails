<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use App\About;
use App\User;
use Session;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = About::all();
        if(Auth::check() && Auth::user()->rol == 'admin'){
            return view('back/about/index')->withAbouts($abouts);
        }else{
            return view('errors.401');
        }
    }

    public function userindex(){
        $userlist = User::all();
        return view('back/users/index')
                ->withUserlist($userlist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                 'nombre' => 'required|max:255'
                ,'texto' => 'required'
            ]
        );

        $about = new About;

        $about->id = Uuid::generate();
        $about->nombre = $request->nombre;
        $about->titulo = $request->titulo;
        $about->texto  = $request->texto;
        $about->user_uuid   = $request->user()->id;

        if($about->save())
        {
            Session::flash('success', "La dada s'ha guardat correctament" );
        }else{
            Session::flash('error', "Problemes al guardar la dada");
        }

        return redirect()->route('about.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'texto' => 'required'
            ]
        );
        $about = About::find($id);

        $about->titulo = $request->titulo;
        $about->texto  = $request->texto;
        $about->user_uuid   = $request->user()->id;

        if($about->save())
        {
            Session::flash('success', "La dada s'ha actualizat correctament" );
        }else{
            Session::flash('error', "Problemes al guardar la dada");
        }

        return redirect()->route('about.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about = About::find($id);
        $about->user()->dissociate();

        $about->delete();

        // Lanzar mensaje de guardado correcto
        Session::flash('success', "Les dades s'han eliminat correctament!");

        // Redirigir a otra pagina
        return redirect()->route('about.index');
    }
}
