<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use App\Servei;
use Session;


class ServeisController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serveis = Servei::orderBy('nombre', 'asc')->get();

        return view('back/serveis/index')
                ->withServeis($serveis);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                 'nombre' => 'required|max:255'
                ,'precio' => 'required|between:0,99.99'
                ,'tiempo' => 'required'
            ]
        );
        $servei = new Servei;
        $servei->id   = Uuid::generate();
        $servei->nombre = $request->nombre;
        $servei->precio = $request->precio;
        $servei->tiempo = $request->tiempo;

        if($servei->save())
        {
            Session::flash('success', "El servei s'ha guardat correctament" );
        }else{
            Session::flash('error', "Problemes al guardar el servei");
        }

        return redirect()->route('serveis.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                 'nombre' => 'required|max:255'
                ,'precio' => 'required|between:0,99.99'
                ,'tiempo' => 'required'
            ]
        );

        $servei = Servei::find($id);
        $servei->nombre = $request->nombre;
        $servei->precio = $request->precio;
        $servei->tiempo = $request->tiempo;

        $servei->save();

        Session::flash('success', "El servei s'ha modificat correctament" );

        return redirect()->route('serveis.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servei = Servei::find($id);
        $servei->delete();

        // Lanzar mensaje de guardado correcto
        Session::flash('success', "El servei s'ha esborrat correctament!");

        // Redirigir a otra pagina
        return redirect()->route('serveis.index');
    }
}
