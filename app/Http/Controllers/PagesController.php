<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use App\Servei;
use App\Imagen;
use App\About;
use Session;
use Mail;

class PagesController extends Controller
{
    public function getCookies()
    {
        return view('front.cookies');
    }

    public function postContact(Request $request)
    {
        $this->validate($request, [
			'email' => 'required|email'
			,'name' => 'required'
			,'phone' => 'required|max:9'
			,'subject' => 'min:3'
			,'message' => 'min:10'
		]);

        $data = array(
			'email' => $request->email
			,'name' => $request->name
			,'phone' => $request->phone
			,'subject' => $request->subject
			,'bodyMessage' => $request->message
		);

        Mail::send('_emails.contactar', $data, function($message) use ($data){
			$message->from($data['email'], 'Rachel\'s Nails :: '.$data['name'])
					->to('rachelnails.net@gmail.com')
					->subject($data['subject']);
		});

        Mail::send('_emails.no-reply_contactar', $data, function($message) use ($data){
			$message->from('rachelnails.net@gmail.com', 'Rachel\'s Nails :: No Reply')
					->to($data['email'])
					->subject('Missatge Rebut '.$data['name'].'!!');
		});

        Session::flash('success', "El missatge s'ha enviat correctament");

		return redirect()->action('PagesController@getContact');
    }

    public function getContact(){
        $dir = About::where('nombre', '=','Dir-local')->first();
        $lnglat = About::where('nombre', '=','LongLat')->first();
        $ttp = About::where('nombre', '=', 'tooltip-map')->first();


        $lnglat = html_entity_decode($lnglat->texto);
        //dd(strip_tags($lnglat));
        // Map Config
        $conf = array(
            'center' => strip_tags($lnglat),
            //'map_type' => 'HYBRID',
            'language' => 'ca_ES',
            '$disableDefaultUI' => true,
            'map_width' => '100%',
            'map_height' => '95%',
            'zoom' => 18
        );

        app('map')->initialize($conf);
        //Gmaps::initialize($config);

        // Colocar marcador
        $marker = array(
            'position' => strip_tags($lnglat),
            'infowindow_content' => $ttp->texto . $dir->texto,
            'animation' => 'BOUNCE'
        );

        app('map')->add_marker($marker);
        //Gmaps::add_marker($marker);

        $map = app('map')->create_map();

        return view('front.contacte')
                ->withMap($map);
    }

    public function getGallery(){
        $imagenes = Imagen::where('activado','=',1)->orderBy('created_at','desc')->paginate(18);
        $categories = Category::orderBy('orderId', 'asc')->get();
        $catCount = array();

        foreach($categories as $i=>$cat){
            $catCount[$cat->nombre] = count($cat->imagenes);
        }

        return view('front.galeria')
                ->withImagenes($imagenes)
                ->withCategories($categories)
                ->withCatCount($catCount);
    }

    public function getPrincipal(){
        $sobre = About::where('nombre', '=', 'Inici-box')->first();
        $serveis = Servei::orderBy('nombre', 'asc')->get();

        return view('front.inicio')
                ->withSobre($sobre)
                ->withServeis($serveis);
    }
}
