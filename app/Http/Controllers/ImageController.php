<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use App\Category;
use App\Servei;
use App\Imagen;
use Session;
use Image;

class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['except' => 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imgs = Imagen::orderBy('created_at','desc')->paginate(12);
        $srvs  = Servei::orderBy('nombre', 'asc')->get();
        $serveis = $this->toArray($srvs);

        $cats = Category::orderBy('orderId', 'asc')->get();
        $categories = $this->toArray($cats);

        $catCount = array();

        foreach($cats as $i=>$cat){
            $catCount[$cat->nombre] = count($cat->imagenes);
        }

        return view('back/galeria/index')
            ->withImgs($imgs)
            ->withCats($cats)
            ->withCategories($categories)
            ->withServeis($serveis)
            ->withCatCount($catCount);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validar los datos!
        $this->validate($request, array(
             'serveis'    => 'required'
            ,'desc'       => 'required'
        ));

        //dd($request->cats_);
        // Guardar en la BDD
        $img = new Imagen;

        $img->id       = Uuid::generate();
        $img->servicio = $request->serveis;
        $img->desc     = $request->desc;
        $img->activado = 0;

        // Guardar imagen
        if($request->hasFile('upImg'))
        {
            $image = $request->file('upImg');
            $filename = $img->id . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/gal/'.$filename);

            // Image::make($image)->resize(400, 800)->save($location);
            // Image::make($image)->save($location);
            $imagen = Image::make($image);
            $imagen->orientate();
            $imagen->resize(1024, null, function($constraint){
                $constraint->upsize();
                $constraint->aspectRatio();
            });
            $imagen->save($location);
            
            $img->filename = $filename;
        }

        $img->save();

        $img->cats()->sync($request->cats_,false);

        Session::flash('success', 'La imatge s\'ha guardat correctament!');
        // Redirigir a otra pagina

        return redirect()->route('imagenes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cats = Category::find($id);
        $imagenes = $cats->imagenes()->where('activado', '=', 1)->orderBy('created_at','desc')->paginate(18);
        $categories = Category::orderBy('orderId', 'asc')->get();
        $catCount = array();

        foreach($categories as $i=>$cat){
            $catCount[$cat->nombre] = count($cat->imagenes);
        }

        return view('front.galeria')
                ->withImagenes($imagenes)
                ->withCategories($categories)
                ->withCatCount($catCount)
                ->withCats($cats);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validar los datos!
        $this->validate($request, array(
            'desc'       => 'required'
        ));

        //dd($request->visSwitch);

        $estado = $request->visSwitch == '1' ? 1 : 0;
        // Guardar en la BDD
        $img = Imagen::find($id);
        $img->servicio = $request->serveis;
        $img->desc = $request->desc;
        $img->activado = $estado;

        $img->save();
        if(isset($request->cats_)){
            $img->cats()->sync($request->cats_);
        }else{
            $img->cats()->sync(array());
        }

        Session::flash('success', 'La imagen se ha modificado correctamente');
        // Redirigir a otra pagina

        return redirect()->route('imagenes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $img = Imagen::find($id);
        $img->cats()->detach();
        $location = public_path('images/gal/'.$img->filename);


        if(File::delete($location)){
            $img->delete();

            Session::flash('success', 'La imatge s\'ha esborrat correctament!');
        }
        // Redirigir a otra pagina
        return redirect()->route('imagenes.index');
    }

    private function toArray($json){
        $temp = [];
        foreach($json as $item){
            $temp[$item->id] = $item->nombre;
        }

        return $temp;
    }
}
