<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use App\Servei;
use App\Cita;
use Session;
use Mail;

class CitasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citas = Cita::all();
        $serveis = Servei::all();
        $srvs = array();
        foreach ($serveis as $servei) {
            $srvs[$servei->id] = $servei->nombre;
        }

        return view('back/citas/index')
                ->withCitas($citas)
                ->withServeis($serveis)
                ->withSrvs($srvs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'dia_cita' => 'required|date_format:d-m-Y'
            ,'hora_inicio' => 'required|before:21:00|after:08:59'
            ,'serveis' => 'required|not_in:0'
        ]);

        $dia_cita = $this->getDiaCitaBD($request->dia_cita);
        $hora_final = $this->getServiceTime($request->serveis, $request->hora_inicio);
        $servei = Servei::find($request->serveis);

        $checkDates = $this->checkDates($dia_cita, $request->hora_inicio, $hora_final);

        if(!$checkDates)
        {
            $cita = new Cita;

            $cita->id         = Uuid::generate();
            $cita->estado       = 0;
            $cita->solicitante  = Auth::user()->id;
            $cita->servicio     = $request->serveis;
            $cita->hora_inicio  = $request->hora_inicio;
            $cita->hora_final   = $hora_final;
            $cita->dia_cita     = $dia_cita;
            $cita->notas        = $request->notas;

            $cita->save();

            $data = array(
    			'email' => Auth::user()->email
    			,'name' =>  Auth::user()->name
    			,'phone' =>  Auth::user()->phone
                ,'id_cita' => $cita->id
                ,'servei' => $servei->nombre
                ,'dia' => $dia_cita
                ,'inici' => $request->hora_inicio
                ,'final' => $hora_final
                ,'notas' => $request->notas
    		);
            Mail::send('_emails.ncita', $data, function($message) use ($data){
    			$message->from($data['email'], 'Rachel\'s Nails')
    					->to('rachelnails.net@gmail.com')
    					->subject('Petició de cita :: ' . $data['name']);
    		});

            Mail::send('_emails.no-reply_ncita', $data, function($message) use ($data){
    			$message->from('rachelnails.net@gmail.com', 'Rachel\'s Nails :: No Reply')
    					->to($data['email'])
    					->subject('Petició de cita :: Rachel\'s Nails');
    		});

            Session::flash('success', "La cita s'ha demanat correctament");
        }else{
            Session::flash('error', "La cita no s'ha demanat perque hi han cites en conflicte");
        }

        $citas = Cita::all();
        $serveis = Servei::all();
        $srvs = array();
        foreach ($serveis as $servei) {
            $srvs[$servei->id] = $servei->nombre;
        }

        return view('back/citas/index')
                ->withCitas($citas)
                ->withServeis($serveis)
                ->withSrvs($srvs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'dia_cita' => 'required|date_format:d-m-Y'
            ,'hora_inicio' => 'required|before:21:00|after:08:59'
            ,'serveis' => 'required|not_in:0'
        ]);


        $dia_cita = $this->getDiaCitaBD($request->dia_cita);

        $hora_final = $this->getServiceTime($request->serveis, $request->hora_inicio);

        $checkDates = $this->checkUpdateDates($dia_cita, $request->hora_inicio, $hora_final, $id);

        $servei = Servei::find($request->serveis);

        if(!$checkDates)
        {
            $cita = Cita::find($id);

            $cita->servicio = $request->serveis;
            $cita->hora_inicio = $request->hora_inicio;
            $cita->hora_final = $hora_final;
            $cita->dia_cita = $dia_cita;
            $cita->notas = $request->notas;

            $data = [];
            $data['email']   = Auth::user()->email;
            $data['name']    = Auth::user()->name;
            $data['phone']   = Auth::user()->phone;
            $data['id_cita'] = $id;
            $data['servei']  = $servei->nombre;
            $data['dia']     = $dia_cita;
            $data['inici']   = $request->hora_inicio;
            $data['final']   = $hora_final;
            $data['notas']   = $request->notas;

            if($request->estado != '99'){
                switch ($request->estado)
                {
                    case 0:
                        $data['accio'] = 'pendent';
                        break;
                    case 1:
                        $data['accio'] = 'acceptat';
                        break;
                    case 2:
                        $data['accio'] = 'cancel·lat';
                        break;
                }
                $cita->estado = $request->estado;

                Mail::send('_emails.mcita', $data, function($message) use ($data){
        			$message->from($data['email'], 'Rachel\'s Nails')
        					->to('rachelnails.net@gmail.com')
        					->subject('Modificació de cita :: ' . $data['name']);
        		});

                Mail::send('_emails.no-reply_mcita', $data, function($message) use ($data){
        			$message->from('rachelnails.net@gmail.com', 'Rachel\'s Nails :: No Reply')
        					->to($data['email'])
        					->subject('Modificació de cita :: Rachel\'s Nails');
        		});
            }

            $cita->save();

            Session::flash('success', "La cita s'ha modificat correctament");
        }else{
            Session::flash('error', "La cita no s'ha pogut modificar");
        }

        $citas = Cita::all();
        $serveis = Servei::all();
        $srvs = array();
        foreach ($serveis as $servei) {
            $srvs[$servei->id] = $servei->nombre;
        }

        return view('back/citas/index')
                ->withCitas($citas)
                ->withServeis($serveis)
                ->withSrvs($srvs);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cita=Cita::find($id);

        $cita->delete();

        Session::flash('success', 'La cita s\'ha eliminat correctament');

        $citas = Cita::all();
        $serveis = Serveis::all();

        return view('back/citas/index')
                ->withCitas($citas)
                ->withServeis($serveis);
    }

    // Funciones propias

    private function getServiceTime($uuid, $hora_inicio)
    {

        $servei = Servei::find($uuid); // Recuperamos el intervalo del servicio
        $dt = Date::parse($servei->tiempo)->format('H \h\o\u\r\s\ \+ i \m\i\n\u\t\e\s'); // Conversion al formato

        return date('H:i:s', (strtotime($hora_inicio . ' + ' . $dt))); // Sumamos las dos horas
    }

    private function getDiaCitaBD($dia_cita)
    {
        return Date::parse($dia_cita)->format('Y-m-d');
    }

    private function checkDates($dia, $hi, $hf)
    {
        $count = Cita::whereRaw(
            "dia_cita='". $dia ."' AND estado=1 "
            ."AND ("
            ."('".$hi."' BETWEEN hora_inicio AND hora_final) OR ('".$hf."' BETWEEN hora_inicio AND hora_final)"
            .")"
            )->count();

        return ($count >= 1 ? true : false);
    }

    private function checkUpdateDates($dia, $hi, $hf, $uuid)
    {
        $count = Cita::whereRaw(
            "dia_cita='". $dia ."' AND estado=1 "
            ."AND id <> '".$uuid."' "
            ."AND ("
            ."('".$hi."' BETWEEN hora_inicio AND hora_final) OR ('".$hf."' BETWEEN hora_inicio AND hora_final)"
            .")"
            )->count();

        return ($count >= 1 ? true : false);
    }
}
