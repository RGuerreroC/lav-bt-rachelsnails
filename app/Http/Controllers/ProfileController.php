<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Webpatser\Uuid\Uuid;
use App\Profile;
use App\Imagen;
use App\User;
use Session;
use Image;

class ProfileController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::id() == $id) {
            $user = User::find($id);
            return view('back/profiles/index')
                    ->withUser($user);
        }else{
            return view('errors/401');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
        [
            'name' => 'required',
            'phone' => 'required|min:9|max:9'
            ,'email' => 'required|email'
        ]);

        $usr = User::find($id);

        $usr->phone = $request->phone;
        $usr->email = $request->email;
        $usr->name = $request->name;

        $usr->save();

        Session::flash('success', "El usuari s'ha modificat correctament" );

        return redirect()->route('p.show', $id);
    }

    public function modImg($id, Request $request)
    {
        $profile = Profile::where('user_id', $id);
        if(isset($profile->id) == null){
            $profile = new Profile;
            $profile->id   = Uuid::generate();
            $profile->user_id = $id;
        }

        // Guardar imagen
        if($request->hasFile('upImg'))
        {
            $img = $request->file('upImg');
            $username = Auth::user()->username;
            $filename = $id . '_' . $username . '.' . $img->getClientOriginalExtension();
            $location = public_path('images/users/'.$filename);

            $imagen = Image::make($img);
            $imagen->orientate();
            $imagen->resize(300, null, function($constraint){
                $constraint->upsize();
                $constraint->aspectRatio();
            });
            $imagen->save($location);
            
            $profile->filename = $filename;

        }
        if($profile->save())
        {
            Session::flash('success', "Imatge guardada correctament!");
        }else{
            Session::flash('error', 'Imatge no guardada');
        }
        return redirect()->route('p.show', $id);
    }

    public function modPass($id, Request $request)
    {
        $this->validate($request,
            [
                 'cur_password' => 'required'
                ,'new_password' => 'required'
                ,'rep_password' => 'required|same:new_password'
            ]
        );
        if(Hash::check($request->cur_password, Auth::user()->password)){
            $user = User::find($id);

            $user->password = Hash::make($request->new_password);

            $user->save();
            Auth::logout();
            Session::flash('success', "Contrasenya modificada, torna a fer login");
            return redirect()->route('login');
        }else{
            Session::flash('error', "La contrasenya no coincideix.");
            return redirect()->route('p.show', $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        if(Hash::check($request->password, Auth::user()->password)){
            $user = User::find($id);

            $user->confirmed = 0;
            $user->save();
            
            Auth::logout();
            Session::flash('success', "Compte donat de baixa correctament! Recorda que tens 30 dies per recuperar el teu compte.");

        }else{
            Session::flash('error', "No es pot donar de baixa aquest compte! Possat en contacte amb nosaltres");
            // Redirigir a otra pagina
            return redirect()->route('contecte.index');
        }
    }
}
