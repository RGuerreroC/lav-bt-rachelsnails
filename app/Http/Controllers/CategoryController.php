<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use App\Category;
use Session;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'nombre' => 'required|min:3|max:255'
        ]);

        $cat = new Category;
        $cat->id = Uuid::generate();
        $cat->nombre = $request->nombre;

        $cat->save();

        Session::flash('success', "Categoria creada correctament" );

        return redirect()->route('imagenes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
        [
            'nombre' => 'required|min:3|max:255'
        ]);

        $cat = Category::find($id);
        $cat->nombre = $request->nombre;

        $cat->save();

        Session::flash('success', "Categoria modificada correctament" );

        return redirect()->route('imagenes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = Category::find($id);
        $cat->imagenes()->detach();

        $cat->delete();

        Session::flash('success', "Categoria esborrada correctament");

        return redirect()->route('imagenes.index');
    }
}
