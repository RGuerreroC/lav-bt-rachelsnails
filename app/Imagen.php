<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    protected $primaryKey = "id";
    public $incrementing = false;
    protected $table = "imagenes";

    protected $fillable = [
        'servicio',
    ];
    public function serveis()
    {
    	return $this->belongsTo('App\Servei','servicio', 'id');
    }

    public function cats()
    {
    	return $this->belongsToMany('App\Category');
    }
}
