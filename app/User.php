<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    //protected $primaryKey = 'uuid';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'id','name', 'username', 'email', 'password', 'confirmation_code',
    ];
    protected $primaryKey = "id";
    public $incrementing = false;
    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function about()
    {
        return $this->hasMany('App\About');
    }

    public function citas()
    {
        return $this->hasMany('App\Cita');
    }

    public function avatar()
    {
        return $this->hasOne('App\Profile');
    }
}
