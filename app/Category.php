<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = "id";
    public $incrementing = false;
    
    public function imagenes()
    {
    	return $this->belongsToMany('App\Imagen');
    }
}
