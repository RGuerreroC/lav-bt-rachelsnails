<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servei extends Model
{
    protected $primaryKey = "id";
    public $incrementing = false;

    public function citas()
    {
        return $this->hasMany('App\Cita');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }
}
