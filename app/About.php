<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $primaryKey = "id";
    public $incrementing = false;
    
	//protected $primaryKey = 'uuid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_uuid',
    ];

	public function user()
    {
    	return $this->belongsTo('App\User','user_uuid', 'id');
    }
}
