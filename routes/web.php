<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middlewareGroups' => ['web']], function(){
    // Authentication Routes
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
    Route::get('logoff', ['as' => 'logoff', 'uses' => 'Auth\LoginController@logout']);
    //                                              Register Routes                                            //
    /***********************************************************************************************************/
    Route::get('registrar', ['as' => 'registrar', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('registrar', ['as' => 'registrar', 'uses' => 'Auth\RegisterController@register']);
    Route::get('registrar/verify/{confirmationCode}', [
         'as' => 'confirmation_path'
        ,'uses' => 'Auth\RegisterController@confirm' ]);
    /***********************************************************************************************************/
    // Resources
    Route::resource('citas', 'CitasController',['except' => 'create']);
    Route::resource('imagenes', 'ImageController');
    Route::resource('about', 'AboutController',['except' => ['create', 'show']]);
    Route::resource('serveis', 'ServeisController',['except' => 'create']);
    Route::resource('cat', 'CategoryController', ['except' => ['index', 'show']]);
    Route::resource('u', 'UserController', ['except' => ['create', 'show']]);
    Route::resource('p', 'ProfileController', ['except' => ['create', 'index']]);

    // Profile Custom
    Route::post('p/{id}/mod-pass',['as' => 'p.mod-pass', 'uses' => 'ProfileController@modPass']);
    Route::post('p/{id}/mod-img',['as' => 'p.mod-img', 'uses' => 'ProfileController@modImg']);

    // checkCatID
    Route::get('cat/{id}', ['as' => 'cat', 'uses' => 'CategoryController@checkCatID']);

    // Map & Contact
    Route::get('contacte', ['as ' => 'contacte', 'uses' => 'PagesController@getContact']);
    Route::post('contacte', ['as ' => 'contacte', 'uses' => 'PagesController@postContact']);

    // Gallery
    Route::get('galeria/cat/{cat}',['as' => 'imagenes.galeria', 'uses' => 'ImageController@show']);
    Route::get('galeria', ['as ' => 'galeria', 'uses' => 'PagesController@getGallery']);

    // Principal
    Route::get('/', ['as ' => 'inici', 'uses' =>  'PagesController@getPrincipal']);

    // Cookies
    Route::get('cookies', ['as ' => 'cookies', 'uses' => 'PagesController@getCookies']);
});
/*Auth::routes();*/
