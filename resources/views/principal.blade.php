<!DOCTYPE html>
<html lang="es">
    @include('_statics.header')
    <body class="container-fluid">
        @include('_statics.messages')
        <div id="content" class="row col-lg-10 offset-lg-1 col-md-11 offset-md-1">
            @include('_statics.nav')
            <section id="main" class="col-lg-12 col-md-12">
                <!-- Componente actual -->
                @yield('content')
            </section>
            @include('_statics.footer')
        </div>
        @include('_statics.cookies')
        <!---->

        @include('_statics.globalJS')

        @yield('scripts')
    </body>
</html>
