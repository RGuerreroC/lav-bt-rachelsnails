@extends(Auth::check() && Auth::user()->rol == 'admin' ? 'nf_principal' : 'principal')

@section('content')
<h3>Galeria {{ isset($cats) && $cats->nombre != "Totes" ? "de $cats->nombre" : "" }}</h3>
<!--div class="d-flex justify-content-center">
    {{-- $imagenes->links( "pagination::bootstrap-4") --}}
</div-->

<div class="d-flex justify-content-end">
    <div class="btn-group dropleft">
        <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Mostrar
        </button>
        <div class="dropdown-menu" id="dd-categories" aria-labelledby="btnGroupDrop1">
            @foreach ($categories as $categoria)
                <a class="dropdown-item d-flex justify-content-between align-items-center" href="{{ route('imagenes.galeria', $categoria->id) }}">{{ $categoria->nombre }} <span class="badge badge-pill badge-primary">{{ $catCount[$categoria->nombre] }}</span> </a>
            @endforeach
        </div>
    </div>
</div>
<hr>
<div class="container">
    <div class="row">
        @foreach ($imagenes as $i=>$imagen)
        <div class="col-lg-2 col-sm-1 h-95">
            <div class="card h-95" style="margin-bottom: 1em">
                @if($imagen->filename != null)
                    <a href="{{ asset('images/gal/'.$imagen->filename) }}" data-fancybox="gallery" data-caption="{{$imagen->desc}}">
                        <img class="card-img-top gal-img" src="{{ asset('images/gal/'.$imagen->filename) }}" alt="{{$imagen->desc}}">
                    </a>
                @endif
                @if ($imagen->filename == null)
                    <div class="card-img-top relleno-img"></div>
                @endif
            </div>
        </div>
        @endforeach
    </div>
    <div class="d-flex justify-content-center">
        {{ $imagenes->links( "pagination::bootstrap-4") }}
    </div>
</div>
@endsection

@section('scripts')
  <script>
    $('[data-fancybox="gallery"]').fancybox({
      'width': 650,
      'height':900,
      'autoSize' : true
    });
  </script>
@endsection
