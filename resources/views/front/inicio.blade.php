@extends(Auth::check() && Auth::user()->rol == 'admin' ? 'nf_principal' : 'principal')

@section('title', '|| Inicio')

@section('content')
<section class="row col-lg-12">
    <div class="col-lg-6 main-page">
        @if($sobre != null)
            <h3 class="col-lg-12">{{ $sobre->titulo }}</h3>
            <hr>
            <p class="col-lg-12">{!! $sobre->texto !!}</p>
        @endif
    </div>
    <div class="col-lg-6 main-page">
        <h3>Serveis</h3>
        <hr>
        <table class="table table-borderless">
            <thead>
                <tr>
                <th>Tipus</th>
                <th>Temps (aprox.)</th>
                </tr>
            </thead>
            <tbody>
                @foreach($serveis as $servei)
                <tr>
                <td>{{ $servei->nombre }}</td>
                @if(Date::parse($servei->tiempo)->format('i') == '00')
                        <td>{{ Date::parse($servei->tiempo)->format('H\h') }}</td>
                    @else
                        @if(Date::parse($servei->tiempo)->format('H') == '00')
                            <td>{{ Date::parse($servei->tiempo)->format('i\m') }}</td>
                        @else
                            <td>{{ Date::parse($servei->tiempo)->format('H\h i\m') }}</td>
                        @endif
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
@endsection
