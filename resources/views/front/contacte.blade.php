@extends(Auth::check() && Auth::user()->rol == 'admin' ? 'nf_principal' : 'principal')

@section('title', '|| Contacte')

@section('content')
    <div class="row">
        <div class="col-lg-7 map-content main-page">
            <h3>Estem a:</h3>
            <hr>
            {!! $map['html'] !!}
        </div>
        <div class="col-lg-5 main-page">
            <h3>Formulari de contacte</h3>
            <hr>
            <form class="" action="{{ url('contacte') }}" method="post">
                {{ csrf_field() }}
                @if(Auth::check())
                    <div class="card">
                        <h4 class="card-header">Contacto</h4>
                        <div class="card-body">
                            <h5 class="card-title">
                                @if(isset(Auth::user()->avatar->filename))
                                    <img class="circle" src={{ asset('images/users/'.Auth::user()->avatar->filename) }}>
                                @else
                                    <img class="circle" src={{ asset('images/user-icon.png') }}>
                                @endif
                                {{ Auth::user()->name }}
                            </h5>
                            <div class="list-group list-group-flush">
                                <div class="row justify-content-between">
                                    <div class="col-lg-6 ">
                                        <i class="fas fa-mobile-alt"></i> {{ Auth::user()->phone }}
                                    </div>
                                    <div class="col-lg-6">
                                        <i class="fas fa-at"></i> {{ Auth::user()->email }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input id="name" name="name" type="hidden" value="{{ Auth::user()->name }}">
                    <input id="email" name="email" type="hidden" value="{{ Auth::user()->email }}">
                    <input id="phone" name="phone" type="hidden" value="{{ Auth::user()->phone }}">
                @else
                    <div class="form-group">
                        <label for="name"><i class="far fa-user-circle"></i> Nom i cognoms</label>
                        <input id="name" name="name" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="phone"><i class="fas fa-mobile-alt"></i> Telefon</label>
                        <input id="phone" name="phone" type="tel" class="form-control" pattern="[0-9]{9}" required>
                    </div>
                    <div class="form-group">
                        <label for="email"><i class="fas fa-at"></i> Email</label>
                        <input id="email" name="email" type="email" class="form-control" required>
                    </div>
                @endif
                <div class="form-group">
                    <label for="subject"><i class="fas fa-pencil-alt"></i> Assumpte</label>
                    <input id="subject" name="subject" type="text" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="message"><i class="far fa-comment-alt"></i> Missatge</label>
                    <textarea id="message" name="message" class="form-control" rows="5" ></textarea>
                </div>
                <button type='submit' class="btn btn-sm btn-outline-rncolor btn-block">
                    <i class="fab fa-telegram-plane"></i> Enviar
                </button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    {!! $map['js'] !!}
@endsection
