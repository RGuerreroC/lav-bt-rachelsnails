@extends(Auth::check() && Auth::user()->rol == 'admin' ? 'nf_principal' : 'principal')

@section('content')
    <div class="h-100">
        <h6 class="text-center mt-3">
            En compliment amb el que es disposa en l'article 22.2 de la Llei 34/2002, d'11 de juliol, de Serveis de la Societat de la Informació i de Comerç Electrònic, aquesta pàgina web li informa, en aquesta secció, sobre la política de recollida i tractament de cookies.
        </h6>
        <hr>
        <h4 class="mt-3">
            Què són les cookies?
        </h4>
        <p>
            Una cookie és un fitxer que es descarrega en el seu ordinador en accedir a determinades pàgines web. Les cookies permeten a una pàgina web, entre altres coses, emmagatzemar i recuperar informació sobre els hàbits de navegació d'un usuari o del seu equip i, depenent de la informació que continguin i de la forma en què utilitzi el seu equip, poden utilitzar-se per reconèixer a l'usuari. 
        </p>
        <hr>
        <h4 class="mt-3">
            Quins tipus de cookies utilitza aquesta pàgina web?
        </h4>
        Aquesta pàgina web utilitza els següents tipus de cookies:
        <div class="list-group">
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-70 justify-content-between">
                    <h5 class="mb-1">Cookies d'anàlisi</h5>
                </div>
                <p class="mb-1">Són aquelles que ben tractades per nosaltres o per tercers, ens permeten quantificar el nombre d'usuaris i així realitzar el mesurament i anàlisi estadística de la utilització que fan els usuaris del servei ofert. Per a això s'analitza la seva navegació a la nostra pàgina web amb la finalitat de millorar l'oferta de productes o serveis que li oferim.</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Cookies tècniques</h5>
                </div>
                <p class="mb-1">Són aquelles que permeten a l'usuari la navegació a través de l'àrea restringida i la utilització de les seves diferents funcions, com per exemple, portar a canvi el procés de compra d'un article.</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Cookies de personalització</h5>
                </div>
                <p class="mb-1">Són aquelles que permeten a l'usuari accedir al servei amb algunes característiques de caràcter general predefinides en funció d'una sèrie de criteris en el terminal de l'usuari com per exemple serien l'idioma o el tipus de navegador a través del com es connecta al servei.</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Cookies publicitàries</h5>
                </div>
                <p class="mb-1">Són aquelles que, ben tractades per aquesta web o per tercers, permeten gestionar de la forma més eficaç possible l'oferta dels espais publicitaris que hi ha a la pàgina web, adequant el contingut de l'anunci al contingut del servei sol·licitat o a l'ús que realitzi de la nostra pàgina web. Per a això podem analitzar els seus hàbits de navegació en Internet i podem mostrar-li publicitat relacionada amb el seu perfil de navegació.</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Cookies de publicitat comportamental</h5>
                </div>
                <p class="mb-1">Són aquelles que permeten la gestió, de la forma més eficaç possible, dels espais publicitaris que, si escau, l'editor hagi inclòs en una pàgina web, aplicació o plataforma des de la qual presta el servei sol·licitat. Aquest tipus de cookies emmagatzemen informació del comportament dels visitants obtinguda a través de l'observació continuada dels seus hàbits de navegació, la qual cosa permet desenvolupar un perfil específic per mostrar avisos publicitaris en funció del mateix.</p>
            </div>
        </div>
        <hr>
        <h4 class="mt-3">
            Desactivar les cookies.
        </h4>
        <p>
            Pot vostè permetre, bloquejar o eliminar les cookies instal·lades en el seu equip mitjançant la configuració de les opcions del navegador instal·lat en el seu ordinador.
            En la majoria dels navegadors web s'ofereix la possibilitat de permetre, bloquejar o eliminar les cookies instal·lades en el seu equip.
            A continuació pot accedir a la configuració dels navegadors webs més freqüents per acceptar, instal·lar o desactivar les cookies:
        </p>
        <ul>
            <li><a href="https://support.google.com/chrome/answer/95647?hl=es">Configurar cookies en Google Chrome</a></li>
            <li><a href="http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9">Configurar cookies en Microsoft Internet Explorer</a></li>
            <li><a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we">Configurar cookies en Mozilla Firefox</a></li>
            <li><a href="https://support.apple.com/es-es/HT201265">Configurar cookies en Safari (Apple)</a></li>
        </ul>
        <hr>
        <h4 class="mt-3">
            Cookies de tercers.
        </h4>
        <p>
            Aquesta pàgina web utilitza serveis de tercers per recopilar informació amb finalitats estadístiques i d'ús de la web. S'usen cookies de DoubleClick per millorar la publicitat que s'inclou en el lloc web. Són utilitzades per orientar la publicitat segons el contingut que és rellevant per a un usuari, millorant així la qualitat d'experiència en l'ús del mateix.
            En concret, usem els serveis de Google Adsense i de Google Analytics per a les nostres estadístiques i publicitat. Algunes cookies són essencials per al funcionament del lloc, per exemple el cercador incorporat.
            El nostre lloc inclou altres funcionalitats proporcionades per tercers. Vostè pugues fàcilment compartir el contingut en xarxes socials com Facebook, Twitter o Google +, amb els botons que hem inclòs a aquest efecte.
        </p>
        <hr>
        <h6>
            Advertiment sobre eliminar cookies.
        </h6>
        <small>
            Vostè pot eliminar i bloquejar totes les cookies d'aquest lloc, però part del lloc no funcionarà o la qualitat de la pàgina web pot veure's afectada.
            Si té qualsevol dubte sobre la nostra política de cookies, pot contactar amb aquesta pàgina web a través dels nostres canals de Contacte.
        </small>
    </div>
@endsection