@extends('errors/error-tmpl')

@section('title', 'Pàgina sense autorització')

@section('num_error', '401')
@section('jumbo-title', 'No tens els permissos per accedir a aquesta pàgina.')
@section('jumbo-subtitle')
    Torna a la pàgina <a href="/">principal</a>.
@endsection
