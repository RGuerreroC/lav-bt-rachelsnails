@extends('errors/error-tmpl')

@section('title', 'Pàgina en mantenimient')

@section('num_error', '500')
@section('jumbo-title', 'Ha hagut algín problema amb el servidor.')
@section('jumbo-subtitle')
    Torna a la pàgina <a href="/">principal</a>.
@endsection
