@extends('errors/error-tmpl')

@section('title', 'Pàgina no trobada')

@section('num_error', '404')
@section('jumbo-title', 'Aquesta no existeix o ha estat eliminada.')
@section('jumbo-subtitle')
    Torna a la pàgina <a href="/">principal</a>.
@endsection
