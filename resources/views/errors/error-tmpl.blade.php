<!DOCTYPE html>
<html lang="es">
    @include('_statics.header')
    <body class="container-fluid">
        <div id="content" class="row col-lg-10 offset-lg-1 col-md-11 offset-md-1">
            <section id="main" class="col-lg-12 col-md-12">
                <!-- Componente actual -->
                <div class="jumbotron main-page">
                    <div class="row h-100 justify-content-center align-content-center">
                        <h1 class="num-error">@yield('num_error')</h1>
                    </div>
                    <h3>@yield('jumbo-title')</h3>
                    <p>@yield('jumbo-subtitle')</p>
                </div>
                <div class="row h-100 justify-content-center align-content-center mb-4">
                    <div class="col-lg-4">
                        <div class="card">
                            <img class="card-img-top" src="{{ asset('images/index.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text"><strong>Contacte: </strong><a href="mailto:rachelnails.net@gmail.com">@rachelnails.net</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        @include('_statics.globalJS')
    </body>
</html>
