@extends('errors/error-tmpl')

@section('title', 'Pàgina en mantenimient')

@section('num_error', '503')
@section('jumbo-title', 'Aquest site esta en mode manteniment.')
@section('jumbo-subtitle', 'Tornarem en breu.')
