@extends('principal')

@section('content')
    <div class="row">
        <div class="col-lg-2 offset-lg-5">
            <div class="card" style="width: 18rem; margin-top: 10rem;">
                <div class="card-body">
                    <h5 class="card-title">Login</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Accedeix al teu compte</h6>
                    {!! Form::open() !!}
                    <div class="card-text">
                        <div class="form-group">
                            {{ Form::label('username', 'Usuari') }}
                            {{ Form::text('username', null, ['class' => 'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{ Form::label('password', 'Contrasenya') }}
                            {{ Form::password('password', ['class' => 'form-control'])}}
                        </div>
                    </div>
                    {{ Form::submit('Accedir', ['class' => 'btn waves-effect waves-light rns-purple']) }}
                    <p>Registra't <a href="{{ route('registrar') }}">aqui</a></p>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
