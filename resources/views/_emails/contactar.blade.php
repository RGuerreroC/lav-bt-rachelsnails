@extends('_emails/_email-tmpl')

@section('title', 'Tens un missatge nou enviat a traves del formulari web')

@section('message')
<div class="message">
    <h4>Missatge rebut</h4>
    <hr>
	<p>
        <i>
            <strong>
                {{ $bodyMessage }}
            </strong>
        </i>
    </p>
    <hr>
    <h5>Enviat per:</h5>
    <ul>
    	<li>{{ $name }}</li>
    	<li>{{ $email }}</li>
    	<li>{{ $phone }}</li>
    </ul>
</div
@endsection
