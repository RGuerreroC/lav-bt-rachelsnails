<!DOCTYPE html>
<html lang="es" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style media="screen">
            body{
                font-family: Helvetica,sans-serif;
                width: 80%;
                margin: 15px auto;
                border: 2px groove grey;
                padding: 35px;
                height: 50rem;
            }
            div.title{
                border: 1px solid #ccc;
                border-radius: 3px;
                width: 50%;
                margin: 15px auto;
                padding: 20px;
                background: #eee;
            }
            div.title h3{
                margin: 3px 0px;
                text-align: center;
            }

            div.message{

            }

            div.message p{
                margin-left: 3%;
            }

            div.message ul li, .contacto ul li{ list-style: none; }
            .contacto ul li{ padding-bottom: 55px }
            .footer{
                height: 204px;
                position: relative;
                top: 336px;
            }
            .footer img{ height: 100%; }
            .contacto, .footer img{ float: left; }
            .ecomsg{
                float: right;
                font-size: 10px;
                width: 250px;
                position: relative;
                top: 170px;
            }
            .ecomsg img{ height: 30px; }

        </style>
    </head>
    <body>
        <div class="title">
            <h3>@yield('title')</h3>
        </div>

        <div class="message">
            @yield('message')
        </div>
        <div class="footer">
            <img src="{{ asset('images/index.jpg')}}" alt="">
            <div class="contacto">
                <ul>
                    <li><strong>Site:</strong><a href="http://rachelnails.dxcodercrew.net"></a>Rachel's Nails</li>
                    <li><strong>Mail:</strong><a href="mailto:rachelnails.net@gmail.com">rachelnails.net@gmail.com</a></li>
                    <li><strong>Telefono:</strong><a href="tel:34661951716">661-951-716</a></li>
                </ul>
            </div>
            <div class="ecomsg">
                <img src="{{ asset('images/eco-icon.png') }}">
                <p>No imprimeixis aquest email, si no es necesari</p>
            </div>
        </div>
    </body>
</html>
