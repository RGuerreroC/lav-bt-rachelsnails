@extends('_emails/_email-tmpl')

@section('title', 'Hem rebut un missatge teu')

@section('message')
    <h4>Missatge rebut</h4>
    <hr>
    <p>
        <i>
            <strong>
                {{ $bodyMessage }}
            </strong>
        </i>
    </p>
    <hr>
    <small>
        <p>En breu et donarem resposta per algun dels medis proporcionats</a></p>
        <p>Aquest és un missatge automatic, si us plau no contesteu ja que no rebrem la resposta.</p>
    </small>
@endsection
