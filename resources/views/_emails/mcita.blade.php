@extends('_emails/_email-tmpl')

@section('title', 'Petició de: ' . $servei)
@section('message')
    <h4>Modificació cita</h4>
    <p>Hem rebut una petició de {{ $name }} modifican una cita per un servei de <i>{{ $servei }}</i>, el dia {{ Date::parse($dia)->format('d-m-Y') }} a les {{ $inici }}</p>
    <p>L'estat de la citas es: {{ $accio }}
    <p>Per acceptar o cancel·lar aquesta cita accedeix a l'<a href="http://rachelnails.dxcodercrew.net/citas">agenda</a></p>
    <hr>
@endsection
