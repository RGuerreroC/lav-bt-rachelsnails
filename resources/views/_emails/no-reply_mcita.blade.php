@extends('_emails/_email-tmpl')

@section('title', 'Petició de: ' . $servei)
@section('message')
    <h4>Hola {{ $name }}</h4>
    <p>L'estat de la petició d'un servei de <i>{{ $servei }}</i> per al dia {{ Date::parse($dia)->format('d-m-Y') }} a les {{ $inici }}</p>
    <p>es: {{ $accio }}</p>
    <p>Si creus que ha hagut cap error o tens qualsevol dubte pots enviar-nos un <a href="tel:+34661951716">whatsapp</a> </p>
    <hr>
    <small>
        <p>Si no has sigut tu qui ha demanat la cita, pots avisar-nos a la pàgina de <a href="http://rachelnails.dxcodercrew.net/contacte">contacte</a></p>
        <p>Aquest és un missatge automatic, si us plau no contesteu ja que no rebrem la resposta.</p>
    </small>
@endsection
