@if(Session::has('success'))
    <div class="container" id="message-alert-auto">
        <div class="row">
            <div class="col">
                <div class="justify-content-center">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4>Èxit!</h4>
                        <hr>
                        <p> {{ Session::get('success') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif(Session::has('error'))
    <div class="container" id="message-alert-auto">
        <div class="row">
            <div class="col">
                <div class="justify-content-center">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4>Error!</h4>
                        <hr>
                        <p> {{ Session::get('error') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif(count($errors) > 0)
    <div class="container" id="message-alert-auto">
        <div class="row">
            <div class="col">
                <div class="justify-content-center">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4>Error!</h4>
                        <hr>
                        <p>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="container" id="message-alert-auto"></div>
@endif
