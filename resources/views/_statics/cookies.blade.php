@if(!Auth::check())
<div class="container" id="message-cookies" style="display: none;">
    <div class="row">
        <div class="col">
            <div class="justify-content-center">
                <div class="alert alert-dark text-white alert-dismissible fade show" role="alert">
                    <div class="row justify-content-between">
                        <h6>Politica de Cookies</h6>
                        <p>Per continuar amb la navegació has d'acceptar la <a href="/cookies">GDPR</a> <a href="#" id="cookiesOK" class="btn btn-sm btn-outline-rncolor ml-3">Acceptar</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif