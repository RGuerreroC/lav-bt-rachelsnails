<nav id="nav" class="navbar navbar-expand-lg navbar-light bg-light col-12">
    <a class="navbar-brand" href="#">Rachel's Nails</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item menu-item {{ Request::is('/') ? 'activado': '' }}">
                <a class="nav-link" href='/'><i class="fas fa-lg fa-home"></i> <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item menu-item {{ Request::is('galeria') ? 'activado': '' }}">
                <a class="nav-link" href='/galeria'><i class="far fa-lg fa-images"></i></a>
            </li>
            <li class="nav-item menu-item {{ Request::is('contacte') ? 'activado': '' }}">
                <a class="nav-link" href="/contacte"><i class="fas fa-lg fa-map-signs"></i></i> </a>
            </li>
            @if(Auth::check())
                @if(Auth::user()->rol == 'admin')
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="admin-menu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-bars"></i>
                        </a>
                        <div id="admMenu" class="dropdown-menu dropdown-menu-right" aria-labelledby="admin-menu">
                            <div class="dropdown-item">{{ Auth::user()->name }}
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('serveis.index') }}">Serveis</a>
                            <a class="dropdown-item" href="{{ route('citas.index') }}">Cites</a>
                            <a class="dropdown-item" href="{{ route('imagenes.index') }}">Imatges</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('about.index') }}">Opcions web</a>
                            <a class="dropdown-item" href="{{ route('u.index') }}">Usuaris</a>
                            <a class="dropdown-item" href="{{ route('p.show', Auth::user()->id) }}">Perfil Usuari</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logoff') }}">Logout</a>
                            <div class="dropdown-divider"></div>
                        </div>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="user-menu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-bars"></i>
                        </a>
                        <div id="usrMenu" class="dropdown-menu dropdown-menu-right" aria-labelledby="user-menu">
                            <a class="dropdown-item" href="{{ route('citas.index') }}">Cites</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('p.show', Auth::user()->id) }}">Perfil Usuari</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logoff') }}">Logout</a>
                            <div class="dropdown-divider"></div>
                        </div>
                    </li>
                @endif
            @else
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">
                    <i class="fas fa-lg fa-power-off"></i>
                </a>
            </li>
            @endif
        </ul>
    </div>
</nav>
