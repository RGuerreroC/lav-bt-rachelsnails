<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/jquery/dist/jquery.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/popper.js/dist/umd/popper.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/@fancyapps/fancybox/dist/jquery.fancybox.js"></script>
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/@fortawesome/fontawesome-free/js/all.js"></script>
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/select2/dist/js/select2.min.js"></script>


<script>
	$('#alert_close').click(function(){
		$( "#message-alert-auto" ).fadeOut( "slow", function() {});
	});
	$(function(){
		$("#message-alert-auto").fadeTo(2000, 500).slideUp(500, function(){
			$("#message-alert-auto").slideUp(750);
		});
	
		var cookies = sessionStorage.getItem('cookies');
		if(cookies == null){
			$('#message-cookies').show();
		}
		
		$("#cookiesOK").click(function(){
			sessionStorage.setItem('cookies', 'OK');
			$("#message-cookies").fadeOut( "slow", function() {});
		});
	});
</script>
<!--script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script-->
