<footer id="footer" class="col-lg-12 row">
    <div class="col-lg-4">
        <div class="text-left">
            <span><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a></span>
            <span>Designed by <a href="http://dxcodercrew.net">DX CoderCrew</a></span>
        </div>
    </div>
    <div class="col-lg-4"></div>
    <div class="col-lg-4">
        <div class="text-right" id="tw-btn">
            <a class="twitter-follow-button"
            href="https://twitter.com/RachelsNails_"
            data-size='large'
            data-lang='ca'
            data-show-count="false">Follow @RachelsNails_</a>
        </div>
    </div>
</footer>
