@extends('nf_principal')

@section('title', '|| Editar dades')

@section('stylesheets')
    <script src="http://cdn.dxcodercrew.net/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
    	tinymce.init({
    		selector: 'textarea'
    		,language: 'ca'
    		,plugins: [
    		"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak code"
    		]
    	});
    </script>
@endsection

@section('content')
    <div class="row col-12 h-100 justify-content-center align-items-center">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-body">
                    <div class="col-12 row justify-content-between">
                        <h3 class="card-title">Dades</h3>
                        <button type="button" class="btn btn-rncolor btn-lg btn-circle-card-title" data-toggle="modal" data-target="#addDadaModal">
                            <i class="fas fa-plus"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="addDadaModal" tabindex="-1" role="dialog" aria-labelledby="addDadaModalLabel" aria-hidden="true">
                            @include('back/about/create')
                        </div>
                    </div>
                    <table class="table table-hover col-12">
                        <thead>
                            <tr>
                                <th>Nom curt</th>
                                <th>Descripció curta</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($abouts as $about)
                                <tr data-toggle="modal" data-target="#about-{{ $about->id }}">
                                    <td>{{ $about->nombre }}</td>
                                    <td>{{ $about->titulo }}</td>
                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="about-{{ $about->id }}" tabindex="-1" role="dialog" aria-labelledby="about-{{ $about->id }}-CenterTitle" aria-hidden="true">
                                    @include('back/about/edit')
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                    <hr>
                </div>
            </div>
        </div>
    </div>
@endsection
