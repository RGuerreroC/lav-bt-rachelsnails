<div class="modal-dialog modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="addDadaModalLabel">Afegir nova dada</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    {!! Form::open(['route'  => 'about.store','method' => 'POST']) !!}
    <div class="modal-body">
        <div class="form-group">
            {{ Form::label('nombre', 'Nom') }}
            {{ Form::text('nombre', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('titulo', 'Descripción corta') }}
            {{ Form::text('titulo', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('texto', '') }}
            {{ Form::textarea('texto', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="modal-footer">
        {{ Form::submit('Guardar canvis', ['class' => 'btn btn-outline-success']) }}
    </div>
    {!! Form::close() !!}
  </div>
</div>
