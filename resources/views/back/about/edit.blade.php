<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="about-{{ $about->id }}-CenterTitle">{{ $about->nombre }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        {{ Form::model($about, ['route' => ['about.update', $about->id], 'method' => 'PUT']) }}
        <div class="modal-body row">
            <div class="col-lg-7">
                <div class="form-group">
                    {{ Form::label('nombre', 'Nom') }}
                    {{ Form::text('nombre', null, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('titulo', 'Descripción corta') }}
                    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('texto', '') }}
                    {{ Form::textarea('texto', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-lg-5">
                <div class="card border-rncolor mb-3">
                    <div class="card-header">
                        <h5>Informació</h5>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <h6>Ultima modificació per:</h6>
        					{{ $about->user->name }}
                        </li>
                        <li class="list-group-item">
                            <h6>Creat el:</h6>
        					{{ Date::parse($about->created_at)->format('j \d\e F \d\e Y H:i:s') }}
                        </li>
                        <li class="list-group-item">
                            <h6>Actual·lizat el: </h6>
                            {{ Date::parse($about->updated_at)->format('j \d\e F \d\e Y H:i:s') }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            {{ Form::submit('Guardar canvis', ['class' => 'btn btn-outline-success']) }}
            <button type="button" class="btn btn-outline-danger btn-circle" data-toggle="modal" data-target="#about-{{ $about->id }}-delete">
                <i class="fas fa-trash"></i>
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="about-{{ $about->id }}-delete" tabindex="-1" role="dialog" aria-labelledby="about-{{ $about->id }}-deleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="about-{{ $about->id }}-CenterTitle">{{ $about->nombre }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>['about.destroy',$about->id], 'method' => 'DELETE']) !!}
            <div class="modal-body">
                La eliminació d'aquest registre es total, si el necesites més endavant hauràs de crear-lo de nou.
                Estàs d'acord?
            </div>
            <div class="modal-footer">
                {{ Form::submit('Eliminar', ['class' => 'btn btn-outline-danger']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
