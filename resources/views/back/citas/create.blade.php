<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="citaCenterTitle">Demanar cita</h5>
        </div>
        {!! Form::open(['route' => 'citas.store', 'files' => 'true']) !!}
        <div class="modal-body">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">
                        @if(isset($user->avatar->filename))
                            <img class="circle" src={{ asset('images/users/'.$user->avatar->filename) }}>
                        @else
                            <img class="circle" src={{ asset('images/user-icon.png') }}>
                        @endif
                        {{ Auth::user()->name }}
                    </h5>
                    <div class="list-group list-group-flush">
                        <div class="row justify-content-between">
                            <div class="col-lg-4 ">
                                <i class="fas fa-mobile-alt"></i> {{ Auth::user()->phone }}
                            </div>
                            <div class="col-lg-8">
                                <i class="fas fa-at"></i> {{ Auth::user()->email }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::hidden('solicitante', Auth::user()->id) }}
            <div class="row">
                <div class="form-group col-lg-6">
                    {{ Form::label('dia_cita', 'Dia de la cita') }}
                    <div class="input-group date" id="diaCitaPicker" data-target-input="nearest">
                        {{ Form::text('dia_cita', null, ['class' => 'form-control datetimepicker-input']) }}
                        <div class="input-group-append" data-target="#diaCitaPicker" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></i></div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    {{ Form::label('hora_inicio', 'Hora d\'inici') }}
                    <div class="input-group date" id="horaCitaPicker" data-target-input="nearest">
                        {{ Form::text('hora_inicio', null, ['class' => 'form-control datetimepicker-input']) }}
                        <div class="input-group-append" data-target="#horaCitaPicker" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <select name="serveis" id="serveis" class="form-control">
    				<option value='0'>Tria una opció</option>
    				@foreach($serveis as $servei)
    				<option value="{{ $servei->id }}">{{ $servei->nombre }}</option>
    				@endforeach
    			</select>
            </div>
            <div class="form-group">
    			{{ Form::label('notas', 'Notas') }}
    			{{ Form::textarea('notas', null, ['class' => 'form-control']) }}
    		</div>
        </div>
        <div class="modal-footer">
            {{ Form::submit('Guardar', ['class' => 'btn btn-outline-success']) }}
        </div>
        {!! Form::close() !!}
    </div>
</div>
