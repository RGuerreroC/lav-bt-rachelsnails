@extends('principal')

@section('title', '|| Cites')

@section('stylesheets')
{{ Html::style('http://cdn.dxcodercrew.net/fullcalendar/dist/fullcalendar.min.css') }}
{{ Html::style('http://cdn.dxcodercrew.net/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css') }}
{{-- Html::style('css/material-fullcalendar.css') --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 h-100 main-page">
			<div class="col-lg-12 row justify-content-center">
				<h3 class="col-lg-6">Demanar cita</h3>
				<button type="button" class="btn btn-rncolor btn-circle--title" data-toggle="modal" data-target="#addCitaModal">
					<i class="fas fa-plus"></i>
				</button>
				<!-- Modal -->
				<div class="modal fade" id="addCitaModal" tabindex="-1" role="dialog" aria-labelledby="addCitaModalLabel" aria-hidden="true">
					@include('back/citas/create')
				</div>
                @foreach ($citas as $i => $cita)
                    <!-- Modal -->
    				<div class="modal fade" id="cita-{{ $cita->id }}-Modal" tabindex="-1" role="dialog" aria-labelledby="cita-{{ $cita->id }}-ModalLabel" aria-hidden="true">
    					@include('back/citas/edit')
    				</div>
                @endforeach
			</div>
			<div id="calendar"></div>
        </div>
    </div>
@endsection

@section('scripts')
<script src='http://cdn.dxcodercrew.net/moment/moment.js'></script>
<script src='http://cdn.dxcodercrew.net/fullcalendar/dist/fullcalendar.min.js'></script>
<script src='http://cdn.dxcodercrew.net/fullcalendar/dist/locale/ca.js'></script>
<script src='http://cdn.dxcodercrew.net/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js'></script>
<script type="text/javascript">

    $('#diaCitaPicker').datetimepicker({
        locale: 'es',
        format: 'DD-MM-YYYY'
    });
    $('#horaCitaPicker').datetimepicker({
        format: 'LT'
    });

</script>
<script>
	$(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            // put your options and callbacks here
            events : [
            @foreach($citas as $cita)
            {
                title : '{{ Auth::user()->rol == 'admin' || $cita->solicitante == Auth::user()->id ? $cita->user->name : 'OCUPAT' }}',
            	start : '{{ $cita->dia_cita }}T{{ $cita->hora_inicio }}',
            	end: '{{ $cita->dia_cita }}T{{ $cita->hora_final }}',
            	id: '{{ $cita->id }}',
            	color: 	@if($cita->solicitante == Auth::user()->id)
			            	@if($cita->estado == 2)
			            		'#f44336'
			            	@elseif($cita->estado == 0)
			            		'#909090'
			            	@else
			            		'#52b7a9'
			            	@endif
			            @else
			            	"{{ $cita->estado == 1 ? '#F5A5EB' : '#909090' }}"
			            @endif
            },
            @endforeach
            ],
            eventClick:  function(event) {
                $('#cita-'+event.id+'-Modal').modal();
            },
            header: {
                left:   'title',
                center: false,
                right:  ''
            },
		    footer: {
		    	right: 'month agendaWeek',
                left: 'prev,next'
		    },
		    aspectRatio: 2.5,
            timeFormat: 'H(:mm)', // uppercase H for 24-hour clock
            //editable: true,

            showNonCurrentDates: false,
		    //defaultView: 'agendaWeek', // Only show week view
		    minTime: '09:00:00', // Start time for the calendar
		    maxTime: '21:00:00', // End time for the calendar
			allDayText: 'Citas',
			themeSystem: 'bootstrap4'
		})
    });
</script>
@endsection
