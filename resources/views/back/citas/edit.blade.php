<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="citaCenterTitle">Editar cita</h5>
        </div>
        {{ Form::model($cita, ['route' => ['citas.update', $cita->id], 'method' => 'PUT']) }}
        <div class="modal-body">
            @if(Auth::user()->rol == 'admin' || $cita->solicitante == Auth::user()->id)
                <div class="row w-95 h-100 justify-content-center align-items-center">
                    <div class="card col-lg-7">
                        <div class="card-body">
                            <h5 class="card-title">
                                @if(isset($user->avatar->filename))
                                    <img class="circle" src={{ asset('images/users/'.$user->avatar->filename) }}>
                                @else
                                    <img class="circle" src={{ asset('images/user-icon.png') }}>
                                @endif
                                {{ $cita->user->name }}
                            </h5>
                            <div class="list-group list-group-flush">
                                <div class="row justify-content-between">
                                    <div class="col-lg-4 ">
                                        <i class="fas fa-mobile-alt"></i> {{ $cita->user->phone }}
                                    </div>
                                    <div class="col-lg-8">
                                        <i class="fas fa-at"></i> {{ $cita->user->email }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card border-rncolor mb-3">
                            <div class="card-header">
                                <h5>Informació</h5>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <h6>Creat el:</h6>
                					{{ Date::parse($cita->created_at)->format('j \d\e F \d\e Y H:i:s') }}
                                </li>
                                <li class="list-group-item">
                                    <h6>Actual·lizat el: </h6>
                                    {{ Date::parse($cita->updated_at)->format('j \d\e F \d\e Y H:i:s') }}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            {{ Form::hidden('solicitante', Auth::user()->id) }}
            <div class="row">
                <div class="form-group col-lg-6">
                    {{ Form::label('dia_cita', 'Dia de la cita') }}
                    <div class="input-group date" id="diaCitaPicker" data-target-input="nearest">
                        {{ Form::text('dia_cita', Date::parse($cita->dia_cita)->format('d-m-Y'), ['class' => 'form-control datetimepicker-input']) }}
                        <div class="input-group-append" data-target="#diaCitaPicker" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></i></div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    {{ Form::label('hora_inicio', 'Hora d\'inici') }}
                    <div class="input-group date" id="horaCitaPicker" data-target-input="nearest">
                        {{ Form::text('hora_inicio', Date::parse($cita->hora_inicio)->format('H:i'), ['class' => 'form-control datetimepicker-input']) }}
                        <div class="input-group-append" data-target="#horaCitaPicker" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        {{ Form::label('serveis','Serveis')}}
                        {{ Form::select('serveis', $srvs, $cita->servicio, ['class'=>'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('estado', $cita->estado == 2 && Auth::user()->rol!='admin' ? 'Estat de la cita' : 'Modificar estat') }}
                        @if($cita->estado == 2 && Auth::user()->rol!='admin')
                            <input type="text" value="Cita cancel·lada" class="form-control" disabled>
                        @else
                            <select name="estado" id="estado" class="form-control">
        						<option value="99">Canviar estat</option>
        						@if(Auth::user()->rol=='admin')
                                    @if($cita->estado != 0)
                                        <option value="0">Pendent</option>
                                    @endif
                                    @if($cita->estado != 1)
                                        <option value="1">Acceptar</option>
                                    @endif
        						@endif
                                    @if($cita->estado != 2)
        						        <option value="2">Cancel·lar</option>
                                    @endif
        					</select>
                        @endif
                    </div>
                </div>
                <div class="form-group col-lg-6">
        			{{ Form::label('notas', 'Notas') }}
        			{{ Form::textarea('notas', null, ['class' => 'form-control']) }}
        		</div>
            </div>
            @else
                <div>
                    <h1>No tens access</h1>
                </div>
            @endif
        </div>
        <div class="modal-footer">
            {{ Form::submit('Guardar', ['class' => 'btn btn-outline-success']) }}
        </div>
        {!! Form::close() !!}
    </div>
</div>
