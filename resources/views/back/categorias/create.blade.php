<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="categoriaCenterTitle">Afegir categoria</h5>
        </div>
        {!! Form::open(['route' => 'cat.store', 'files' => 'true']) !!}
        <div class="modal-body">
            <div class="form-group">
                {{ Form::label('nombre', 'Nom Categoria', ['id' => 'addCatForm']) }}
                {{ Form::text('nombre', null, ['id' => 'catNombre', 'list' => 'categorias', 'class' => 'form-control']) }}
                <datalist id="categorias">
                    @foreach ($cats as $i => $categoria)
                        <option id="{{ $categoria->id }}" name="" value="{{ $categoria->nombre }}">
                    @endforeach
                </datalist>
            </div>
        </div>
        <div class="modal-footer">
            {{ Form::submit('Guardar', ['id' => 'btnSubmit', 'class' => 'btn btn-outline-success']) }}
        </div>
        {!! Form::close() !!}
    </div>
</div>
