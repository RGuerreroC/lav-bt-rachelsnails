<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="categoria-{{ $categoria->id }}-CenterTitle">{{ $categoria->nombre }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        {{ Form::model($categoria, ['route' => ['cat.update', $categoria->id], 'method' => 'PUT']) }}
        <div class="modal-body row">
            <div class="col-lg-7">
                {{ Form::label('nombre', 'Nom Categoria') }}
                {{ Form::text('nombre', null, ['list' => 'categorias', 'class' => 'form-control']) }}
                <datalist id="categorias">
                    @foreach ($cats as $i => $cat)
                        <option id="{{ $cat->id }}" name="" value="{{ $cat->nombre }}">
                    @endforeach
                </datalist>
            </div>
            <div class="col-lg-5">
                <div class="card border-rncolor mb-3">
                    <div class="card-header">
                        <h5>Informació</h5>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <h6>Creat el:</h6>
        					{{ Date::parse($categoria->created_at)->format('j \d\e F \d\e Y H:i:s') }}
                        </li>
                        <li class="list-group-item">
                            <h6>Actual·lizat el: </h6>
                            {{ Date::parse($categoria->updated_at)->format('j \d\e F \d\e Y H:i:s') }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            {{ Form::submit('Guardar canvis', ['class' => 'btn btn-outline-success']) }}
            <button type="button" class="btn btn-outline-danger btn-circle" data-toggle="modal" data-target="#categoria-{{ $categoria->id }}-delete">
                <i class="fas fa-trash"></i>
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="categoria-{{ $categoria->id }}-delete" tabindex="-1" role="dialog" aria-labelledby="categoria-{{ $categoria->id }}-deleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="categoria-{{ $categoria->id }}-CenterTitle">{{ $categoria->nombre }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>['cat.destroy',$categoria->id], 'method' => 'DELETE']) !!}
            <div class="modal-body">
                La eliminació d'aquest registre es total, si el necesites més endavant hauràs de crear-lo de nou.
                Estàs d'acord?
            </div>
            <div class="modal-footer">
                {{ Form::submit('Eliminar', ['class' => 'btn btn-outline-danger']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
