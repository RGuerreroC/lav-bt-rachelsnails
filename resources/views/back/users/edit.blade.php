<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="user-{{ $user->id }}-ModalLabel">Modificar usuari</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        {!! Form::model($user,['route' => ['u.update', $user->id],'method' => 'PUT']) !!}
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-group">
                        {{ Form::label('name', 'Nom') }}
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('username', 'Descripción corta') }}
                        {{ Form::text('username', null, ['class' => 'form-control', 'disabled']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('phone', 'Telefon') }}
                        {{ Form::text('phone', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'e-Mail') }}
                        {{ Form::text('email', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group text-center">
                        <h6>Administrador del site</h6>

                        <span class="switch">
                            <label for="switch-{{ $user->id }}-admin">No</label>
                            <input type="checkbox" class="switch" name="adminSwitch" id="switch-{{ $user->id }}-admin" {{ $user->rol == 'admin' ? 'checked' : '' }}>
                            <label for="switch-{{ $user->id }}-admin">Si</label>
                        </span>
                    </div>
                    <div class="form-group text-center">
                        <h6>Confirmació de email</h6>

                        <span class="switch">
                            <label for="switch-{{ $user->id }}-email">No</label>
                            <input type="checkbox" class="switch" name="emailSwitch" id="switch-{{ $user->id }}-email" {{ $user->confirmed == 1 ? 'checked' : '' }}>
                            <label for="switch-{{ $user->id }}-email">Si</label>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 align-content-center">
                    @if(isset($user->avatar->filename))
                        <img class="usr-img" src={{ asset('images/users/'.$user->avatar->filename) }}>
                    @else
                        <img class="usr-img" src={{ asset('images/user-icon.png') }}>
                    @endif
                </div>
            </div>
        </div>
        <div class="modal-footer">
            {{ Form::submit('Guardar canvis', ['class' => 'btn btn-outline-success']) }}
        </div>
        {!! Form::close() !!}
    </div>
</div>
