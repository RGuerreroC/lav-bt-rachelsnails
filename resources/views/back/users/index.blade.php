@extends('nf_principal')

@section('title', '|| Gestió d\'usuaris')

@section('stylesheets')
    <script src="http://cdn.dxcodercrew.net/tinymce/tinymce.min.js"></script>

    <script type="text/javascript">
    	tinymce.init({
    		selector: 'textarea'
    		,language: 'ca'
    		,plugins: [
    		"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak code"
    		]
    	});
    </script>
@endsection

@section('content')
    <div class="row col-12 h-100 justify-content-center align-items-center">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-body">
                    <div class="col-12 row justify-content-between">
                        <h3 class="card-title">Usuaris</h3>
                        <button type="button" class="btn btn-rncolor btn-lg btn-circle-card-title" data-toggle="modal" data-target="#addDadaModal">
                            <i class="fas fa-plus"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="addDadaModal" tabindex="-1" role="dialog" aria-labelledby="addDadaModalLabel" aria-hidden="true">

                        </div>
                    </div>
                    <div class="row row justify-content-between align-content-center">
                        <table class="table table-striped table-hover col-lg-5">
                            <thead>
                                <tr class="text-center">
                                    <th>Nom <i class="fas fa-chess-king"></i></th>
                                    <th>Estat usuari</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($userlist as $user)
                                    @if($user->rol == 'admin')
                                    <tr data-toggle="modal" data-target="#user-{{ $user->id }}">
                                        <td class=""><i class="{{ $user->username == 'RaulGC' ? 'fas fa-user-ninja' : ''}}"></i>{{ $user->username }}</td>
                                        <td class="text-center">
                                            <i class="{{ $user->confirmed == 1 ? 'fas' : 'far' }}  fa-star"</i>
                                        </td>
                                    </tr>
                                    <!-- Modal -->
                                    <div class="modal fade" id="user-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="user-{{ $user->id }}-CenterTitle" aria-hidden="true">
                                        @include('back/users/edit')
                                    </div>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                        <table class="table table-striped table-hover col-lg-5 offset-lg-2">
                            <thead>
                                <tr class="text-center">
                                    <th>Nom <i class="fas fa-user"></i></th>
                                    <th>Estat usuari</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($userlist as $user)
                                    @if($user->rol != 'admin')
                                    <tr data-toggle="modal" data-target="#user-{{ $user->id }}">
                                        <td class=""><i class="{{ $user->username == 'RaulGC' ? 'fas fa-user-ninja' : ''}}"></i>{{ $user->username }}</td>
                                        <td class="text-center">
                                            <i class="{{ $user->confirmed == 1 ? 'fas' : 'far' }}  fa-star"</i>
                                        </td>
                                    </tr>
                                    <!-- Modal -->
                                    <div class="modal fade" id="user-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="user-{{ $user->id }}-CenterTitle" aria-hidden="true">
                                        @include('back/users/edit')
                                    </div>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
