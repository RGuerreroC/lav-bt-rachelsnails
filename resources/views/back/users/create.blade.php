<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="user-{{ $user->id }}-ModalLabel">Modificar usuari</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <h1>Opció temporalment no disponible</h1>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>
