<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="servei-{{ $servei->id }}-CenterTitle">{{ $servei->nombre }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        {{ Form::model($servei, ['route' => ['serveis.update', $servei->id], 'method' => 'PUT']) }}
        <div class="modal-body">

            <div class="form-group">
                {{ Form::label('nombre', 'Nom') }}
                {{ Form::text('nombre', null, ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('precio', 'Preu') }}
                {{ Form::text('precio', null, ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('tiempo', 'Temps') }}
                {{ Form::time('tiempo', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="modal-footer">
            {{ Form::submit('Guardar canvis', ['class' => 'btn btn-outline-success']) }}
            <button type="button" class="btn btn-outline-danger btn-circle" data-toggle="modal" data-target="#servei-{{ $servei->id }}-delete">
                <i class="fas fa-trash"></i>
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="servei-{{ $servei->id }}-delete" tabindex="-1" role="dialog" aria-labelledby="servei-{{ $servei->id }}-deleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="servei-{{ $servei->id }}-CenterTitle">{{ $servei->nombre }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>['serveis.destroy',$servei->id], 'method' => 'DELETE']) !!}
            <div class="modal-body">
                La eliminació d'aquest registre es total, si el necesites més endavant hauràs de crear-lo de nou.
                Estàs d'acord?
            </div>
            <div class="modal-footer">
                {{ Form::submit('Eliminar', ['class' => 'btn btn-outline-danger']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
