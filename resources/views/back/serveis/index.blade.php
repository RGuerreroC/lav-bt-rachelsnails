@extends('nf_principal')

@section('title', '|| Serveis')

@section('content')
    <div class="row col-12 h-100 justify-content-center align-items-center">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Serveis</h3>
                    <table class="table table-hover col-12">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Preu</th>
                                <th>Temps (aprox)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($serveis as $servei)
                                <tr data-toggle="modal" data-target="#servei-{{ $servei->id }}">
                                    <td>{{ $servei->nombre }}</td>
                                    <td class="center">{{ number_format($servei->precio,2,',','.') }}€</td>
                                    @if(Date::parse($servei->tiempo)->format('i') == '00')
                                        <td class="center">{{ Date::parse($servei->tiempo)->format('H\h') }}</td>
                                    @else
                                        @if(Date::parse($servei->tiempo)->format('H') == '00')
                                            <td class="center">{{ Date::parse($servei->tiempo)->format('i\m') }}</td>
                                        @else
                                            <td class="center">{{ Date::parse($servei->tiempo)->format('H\h i\m') }}</td>
                                        @endif
                                    @endif
                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="servei-{{ $servei->id }}" tabindex="-1" role="dialog" aria-labelledby="servei-{{ $servei->id }}-CenterTitle" aria-hidden="true">
                                    @include('back/serveis/edit')
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-3 offset-lg-1">
            <div class="card">
                {!! Form::open(['route' => 'serveis.store', 'method' => 'POST']) !!}
                <div class="card-body">
                    <h3 class="card-title">Afegir servei</h3>
                    <hr>
                    <div class="form-group">
                        {{ Form::label('nombre', 'Nom') }}
                        {{ Form::text('nombre', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('precio', 'Preu') }}
                        {{ Form::text('precio', null, ['class' => 'form-control']) }}
        			</div>
        			<div class="form-group">
        				{{ Form::label('tiempo', 'Temps') }}
        				{{ Form::time('tiempo', null, ['class' => 'form-control']) }}
        			</div>
                    <div class="form-group text-center">
                        {{Form::button(
        					'<i class="fas fa-plus"></i>'
        					,array(
        						'type' => 'submit'
        						,'class' => 'btn btn-lg btn-outline-rncolor btn-circle'
        					)
        				)}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
