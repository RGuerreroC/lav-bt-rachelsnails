@extends('nf_principal')

@section('title', '|| Gestió galeria')

@section('stylesheets')
<script src="http://cdn.dxcodercrew.net/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
	tinymce.init({
		selector: 'textarea'
		,language: 'ca'
		,plugins: [
			"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak code"
		]
	});
</script>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 main-page">
			<div class="col-lg-12 row justify-content-between">
	            <h3 class="col-lg-3 col-sm-12">Imatges</h3>
	            <div class="align-self-center">
	                {{ $imgs->links("pagination::bootstrap-4-custom") }}
	            </div>
				<button type="button" class="btn btn-rncolor btn-circle--title" data-toggle="modal" data-target="#addDadaModal">
					<i class="fas fa-plus"></i>
				</button>
				<!-- Modal -->
				<div class="modal fade" id="addDadaModal" tabindex="-1" role="dialog" aria-labelledby="addDadaModalLabel" aria-hidden="true">
					@include('back/galeria/create')
				</div>
			</div>
            <hr>
            <div class="card-columns">
                @foreach($imgs as $i=>$imagen)
                <div class="card bg-dark text-white h-90" data-toggle="modal" data-target="#imagen-{{ $imagen->id }}">
                    <img class="card-img-top gal-img {{ $imagen->activado == 0 ? 'img-disabled' : '' }}" src="{{ asset('images/gal/'.$imagen->filename) }}" alt="{{$imagen->desc}}" >
                    <div class="card-img-overlay">
                        <h5 class="card-title">{{ $imagen->serveis->nombre }}</h5>
                    </div>
                </div>
                <div class="modal fade" id="imagen-{{ $imagen->id }}" tabindex="-1" role="dialog" aria-labelledby="imagen-{{ $imagen->id }}-Label" aria-hidden="true">
                    @include('back/galeria/edit')
                </div>
            @endforeach
            </div>
        </div>
		<div class="col-lg-3 offset-lg-1 main-page-cat">
			<div class="card">
				<div class="card-body">
					<div class="col-lg-12 justify-content-between row">
						<button type="button" class="btn btn-rncolor btn-sm btn-circle-card-title-left" data-toggle="modal" data-target="#addCatModal">
							<i class="fas fa-plus"></i>
						</button>
						<h4 class="card-title">Categories</h4>
						<!-- Modal -->
						<div class="modal fade" id="addCatModal" tabindex="-1" role="dialog" aria-labelledby="addCatModalLabel" aria-hidden="true">
							@include('back/categorias/create')
						</div>
					</div>
				</div>
				<ul class="list-group">
					@foreach ($cats as $i => $categoria)
							<!--li class="list-group-item">{{-- $categoria->nombre --}}</li-->
							@if($categoria->nombre == 'Totes')
								<div class="list-group-item d-flex justify-content-between align-items-center">
								    {{ $categoria->nombre }}
								    <span class="badge badge-primary badge-pill">{{ $catCount[$categoria->nombre] }}</span>
								</div>
							@else
								<a href="#" class="list-group-item d-flex justify-content-between align-items-center" data-toggle="modal" data-target="#categoria-{{ $categoria->id }}-Modal">
								    {{ $categoria->nombre }}
								    <span class="badge badge-primary badge-pill">{{ $catCount[$categoria->nombre] }}</span>
								</a>
							@endif
							<!-- Modal -->
							<div class="modal fade" id="categoria-{{ $categoria->id }}-Modal" tabindex="-1" role="dialog" aria-labelledby="categoria-{{ $categoria->id }}-ModalLabel" aria-hidden="true">
								@include('back/categorias/edit')
							</div>
					@endforeach
				</ul>
			</div>
		</div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
		$('#btnImage').on('change', function(){
	        var fileInput = this;
	        if(fileInput.files[0])
	        {
	            var rdr = new FileReader();
	            rdr.onload=function(e)
	            {
	                $("#prevImg").attr('src', e.target.result);
	                $("#prevImg-container").css('display', 'block');
	            }
	            rdr.readAsDataURL(fileInput.files[0]);
	        }
	    });
    });

    function imageChanger(e){
        e.children.visSwitch.value = (e.children.visSwitch.value == 0) ? 1 : 0;
        if(e.children.visSwitch.value == 0){
            e.children.prevImg.classList.add('img-disabled');
        }else{
            e.children.prevImg.classList.remove('img-disabled');
        }
    }


</script>
@endsection
