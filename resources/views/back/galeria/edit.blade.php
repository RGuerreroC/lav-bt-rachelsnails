<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="about-{{ $imagen->id }}-CenterTitle">{{ $imagen->serveis->nombre }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        {{ Form::model($imagen, ['route' => ['imagenes.update', $imagen->id], 'method' => 'PUT']) }}
        <div class="modal-body row">
            <div class="col-lg-7">
                <div class="form-group">
                    {{ Form::label('serveis','Serveis') }}
                    {{ Form::select('serveis', $serveis, $imagen->servicio, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    <label for="cats">
                        Categories
                        {{ Form::select('cats []', $categories, $imagen->cats, ['class' => 'js-example-basic-multiple', 'multiple']) }}
                    </label>
                </div>
                <div class="form-group">
                    {{ Form::label('desc', 'Descripció') }}
                    {{ Form::textarea('desc', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-lg-5">
                <div class="card bg-dark text-white" onClick="imageChanger(this)">
                    <img id="prevImg" class="card-img-top {{ $imagen->activado == 0 ? 'img-disabled' : '' }}" src="{{ asset('images/gal/'.$imagen->filename) }}" alt="{{$imagen->desc}}" >
                    <input type="hidden" name="visSwitch" id="visSwitch" value="{{ $imagen->activado == 0 ? 0 : 1 }}">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            {{ Form::submit('Guardar canvis', ['class' => 'btn btn-outline-success']) }}
            <button type="button" class="btn btn-outline-danger btn-circle" data-toggle="modal" data-target="#imagen-{{ $imagen->id }}-delete">
                <i class="fas fa-trash"></i>
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="imagen-{{ $imagen->id }}-delete" tabindex="-1" role="dialog" aria-labelledby="imagen-{{ $imagen->id }}-deleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imagen-{{ $imagen->id }}-CenterTitle">Eliminar imagen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>['imagenes.destroy',$imagen->id], 'method' => 'DELETE']) !!}
            <div class="modal-body">
                La eliminació d'aquesta imatge es total, si la necesites més endavant hauràs de crear-la de nou.
                Estàs d'acord?
            </div>
            <div class="modal-footer">
                {{ Form::submit('Eliminar', ['class' => 'btn btn-outline-danger']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
