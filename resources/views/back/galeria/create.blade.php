<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="imagenCenterTitle">Afegir imatge</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        {!! Form::open(['route' => 'imagenes.store', 'files' => 'true']) !!}
        <div class="modal-body row justify-content-between">
            <div class="col-12 d-flex justify-content-end">
                <label class="btn btn-default btn-outline-rncolor btn-circle ">
                    <i class="fas fa-camera-retro"></i>
                    {{ Form:: file('upImg',['id' => "btnImage", 'style' => 'display:none']) }}
                </label>
            </div>
            <div class="col-lg-7">
                <div class="form-group">
                    {{ Form::label('serveis','Serveis') }}
                    {{ Form::select('serveis', $serveis, null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    <label for="cats">
                        Categories
                        {{ Form::select('cats []', $categories, 'Totes', ['class' => 'js-example-basic-multiple', 'multiple']) }}
                    </label>
                </div>
                <div class="form-group">
                    {{ Form::label('desc', 'Descripció') }}
                    {{ Form::textarea('desc', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-lg-5" id="prevImg-container" style="display: none;">
                <div class="card bg-dark text-white" >
                    <img id="prevImg" class="card-img-top" src="" >
                </div>
            </div>
        </div>
        <div class="modal-footer">
            {{ Form::submit('Guardar', ['class' => 'btn btn-outline-success']) }}
        </div>
        {{ Form::close() }}
    </div>
</div>
