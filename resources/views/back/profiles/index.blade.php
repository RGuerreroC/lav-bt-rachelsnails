@extends(Auth::user()->rol == 'admin' ? 'nf_principal' : 'principal')

@section('title', '|| Profile')

@section('content')
<div class="row col-12 h-100 justify-content-center align-items-center">
    <div class="col-lg-9">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Dades usuari: {{ $user->name }}</h5>
                <div class="d-flex justify-content-between align-content-center">
                    <div class="card mr-1 ml-1 flex-grow-1">
                        <div class="card-body">
                            {{ Form::model($user, ['route' => ['p.update', $user->id], 'method' => 'PUT']) }}
                                <h6 class="card-title">Dades personals</h6>
                                <div class="form-group">
                                    {{ Form::label('username', 'Nom d\'usuari') }}
                                    {{ Form::text('username', null, ['class' => 'form-control', 'disabled']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('name', 'Nom complet') }}
                                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('email', 'e-Mail') }}
                                    {{ Form::text('email', null, ['class' => 'form-control']) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('phone', 'Telefon') }}
                                    {{ Form::text('phone', null, ['class' => 'form-control']) }}
                                </div>
                                 <hr>
                                <div class="form-group row justify-content-center">
                                    {{ Form::submit('Modificar', ['class' => 'btn btn-rncolor'])}}
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <div class="card mr-1 ml-1 align-self-start">
                        {{ Form::open(['route' => ['p.mod-img', $user->id], 'files' => 'true'])}}
                        <div class="card-body">
                            <h6 class="card-title">Imatge</h6>
                            <label for="btnUserImg">
                                @if(isset($user->avatar->filename))
                                    <img class="usr-img" id="prevUserImg" src={{ asset('images/users/'.$user->avatar->filename) }}>
                                @else
                                    <img class="usr-img" id="prevUserImg" src={{ asset('images/user-icon.png') }}>
                                @endif
                            </label>
                            {{ Form:: file('upImg',['id' => "btnUserImg", 'style' => 'display:none']) }}
                            <hr>
                            <div class="form-group row justify-content-center">
                                {{ Form::submit('Modificar', ['class' => 'btn btn-rncolor'])}}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                    <div class="card mr-1 ml-1 align-self-start">
                        <div class="card-body">
                        <h6 class="card-title">Cambiar contrasenya</h6>
                        {!! Form::open(['route' => ['p.mod-pass', $user->id], 'method' => 'POST']) !!}
                            <div class="form-group">
                                {{ Form::password('cur_password', ['class' => 'form-control', 'placeholder' => 'Contrasenya actual']) }}
                                {{ Form::password('new_password', ['class' => 'form-control', 'placeholder' => 'Nova contrasenya']) }}
                                {{ Form::password('rep_password', ['class' => 'form-control', 'placeholder' => 'Repeteix Contrasenya']) }}
                            </div>
                            <hr>
                            <div class="form-group row justify-content-center">
                                {{ Form::submit('Modificar', ['class' => 'btn btn-rncolor'])}}
                            </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card border-danger">
            <div class="card-header bg-danger text-white">
                <h5>Zona perillosa</h5>
            </div>
            <div class="card-body">
                <h6>Donar de baixa aquest compte</h6>
                <button type="button" class="btn btn-outline-danger btn-circle align-self-center" data-toggle="modal"
                    data-target="#user-{{ $user->id }}-delete">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="user-{{ $user->id }}-delete" tabindex="-1" role="dialog" aria-labelledby="user-{{ $user->id }}-deleteLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="about-{{ $user->id }}-CenterTitle">{{ $user->name }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>['p.destroy', $user->id], 'method' => 'DELETE']) !!}
            <div class="modal-body">
                <p>Conservarem les dades durant 30 dies, passat aquest temps no es podrá recuperar el compte.</p>
                {{ Form::label('password', "Introdueïx la teva contrasenya per verificar l'eliminació.") }}
                {{ Form::password('password', ['class' => 'form-control', 'required']) }}
            </div>
            <div class="modal-footer">
                {{ Form::submit('Eliminar', ['class' => 'btn btn-outline-danger']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
		$('#btnUserImg').on('change', function(){
	        var fileInput = this;
	        if(fileInput.files[0])
	        {
	            var rdr = new FileReader();
	            rdr.onload=function(e)
	            {
	                $("#prevUserImg").attr('src', e.target.result);
	            }
	            rdr.readAsDataURL(fileInput.files[0]);
	        }
	    });
    });
</script>
@endsection